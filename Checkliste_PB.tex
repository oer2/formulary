\documentclass[ngerman,a4paper]{THartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}

\usepackage[table]{xcolor}
\usepackage{booktabs,tabularx}
\usepackage{siunitx}
\usepackage{hyperref}

\sisetup{
  output-decimal-marker = {,},
  round-mode            = places
}
\DeclareSIUnit\litre{l}

\newcolumntype{C}{>{$}c<{$}}
\newcolumntype{L}{>{$}l<{$}}
\newcolumntype{R}{>{$}r<{$}}
% \newcolumntype{U}{>{\unit{}c{}}}


\newlength{\formulawidth}
\newlength{\symboltablewidth}
\setlength{\symboltablewidth}{.6\textwidth}
\setlength{\formulawidth}{\textwidth}
\addtolength{\formulawidth}{-\symboltablewidth}
\addtolength{\formulawidth}{-\hangindent}

\setlength{\parindent}{0pt}
\setlength{\parskip}{\smallskipamount}

\definecolor{boxbgcolor}{gray}{0.9}
\newsavebox{\stablebox}
\newenvironment{symboltab}{%
  \sisetup{round-mode = places}%
  \begin{lrbox}{\stablebox}\tabular{Cll}}%
  {\endtabular\end{lrbox}%
  \fboxsep=0pt\colorbox{boxbgcolor}{%
    \begin{minipage}[t]{\symboltablewidth}\usebox\stablebox\end{minipage}\par\bigskip}%
}

\newenvironment{formula}[1]{%
  \def\theformula{#1}%
  \sisetup{round-mode=none}%
  \par\noindent%
  \begin{minipage}[t]{\formulawidth}%
    \qquad
    \begin{math}\displaystyle\theformula\end{math}%
  \end{minipage}%
  \begin{symboltab}}{%
  \end{symboltab}%
}


% quantities 
\newcommand{\eulernumberqty}[1][3]{\num[round-mode=places,round-precision=#1]{%
  2.7182818284590452353602874713526624977572470936999595749669676277}}
\newcommand{\piqty}[1][4]{\num[round-precision=#1]{%
  3.1415926535 8979323846 2643383279 5028841971 6939937510
       5820974944 5923078164 0628620899 8628034825 3421170679}}

% https://physics.nist.gov/cuu/Constants/Table/allascii.txt
\newcommand{\atomicmassqty}[1][2]{%
  \qty[round-precision=#1]{1.660 539 066 60 e-27}{\kilogram}}
\newcommand{\avogadroconstantqty}[1][3]{%
  \qty[round-precision=#1]{6.022 140 76 e23}{\per\mole}}
\newcommand{\elementarychargeqty}[1][2]{%
  \qty[round-precision=#1]{1.602 176 634 e-19}{\coulomb}}
\newcommand{\molargasconstantqty}[1][1]{%
  \qty[round-precision=#1]{8.314 462 618}{\joule\per\mole\per\kelvin}}
\newcommand{\planckconstantqty}[1][2]{%
  \qty[round-precision=#1]{6.62607015e-34}{\joule\second}}
\newcommand{\speedoflightqty}[1][-3]{%
  \qty[round-precision=#1]{299 792.458}{\kilo\metre\per\second}}
\newcommand{\bohrradiusqty}[1][0]{%
  \qty[round-precision=#1]{5.291 772 109 03 e-11}{\metre}}
\newcommand{\standardatmosphereqty}[1][0]{%
  \qty[round-precision=#1]{1013.25}{\hecto\pascal}}
\newcommand{\molargasvolumeqty}[1][1]{%
  \qty[round-precision=#1]{22.413 969 54 e-3}{\metre\cubed\per\mole}}
\newcommand{\standardgravityacceleration}[1][2]{%
  \qty[round-precision=#1]{9,806 65}{\metre\per\second\squared}}
\newcommand{\vacuumelectricpermittivityqty}[1][1]{%
  \qty[round-precision=#1]{8.854 187 8128 e-12}{\ampere\second\per\volt\per\metre}}

% definitions

\newcommand{\pidef}[1][4]{Kreiszahl ($\pi \approx \piqty{#1}$)}
\newcommand{\eulernumberdef}[1][3]{Basis des natürlichen Logarithmus ($e \approx \eulernumberqty{3}$)}

\newcommand{\earthaccelerationdef}[1][2]{Fallbeschleunigung
  ($g \approx \standardgravityaccelerationqty$ auf \ang{45} nördl. oder südl. Breite und Meereshöhe)}
\newcommand{\planckconstantdef}[1][2]{($h \approx \planckconstantqty{#1}$)}


\title{Checkliste PharmBasics}
\author{Richard Hirsch}


\begin{document}

\maketitle

\section{SI-System}
\label{sec:si}

\subsection{Maßzahl und Einheit}

\begin{formula}{G = \{G\} \cdot [G]}
      G & physikalische Größe & \qty{-273.15}{\celsius} \\
      \{G\} & Maßzahl & \num{-273.15} \\ \relax
      [G]   & Einheit & \hfill\unit{\celsius}
\end{formula}
    

\subsection{SI-Basisgrößen und -einheiten}

\begin{center}
\begin{tabular}{lClc}
  \toprule
  Größe & \text{Formelzeichen} & Name & Einheit \\
  \midrule
  Länge & l, s & Meter & \unit{\metre} \\
  Masse & m    & Kilogramm & \unit{\kilogram} \\
  Zeit  & t    & Sekunde   & \unit{\second} \\
  elektrische Stromstärke & I & Ampere & \unit{\ampere} \\
  thermodynamische Temperatur & T & Kelvin & \unit{\kelvin} \\
  Stoffmenge & \nu, n & Mol & \unit{\mole} \\
  Lichtstärke & I_V & Candela & \unit{\candela} \\
  \bottomrule
\end{tabular}
\end{center}


\newpage
\subsection{Abgeleitete Größen}

\begin{center}
  \sisetup{
    display-per-mode = symbol,
    inline-per-mode = symbol
  }
\begin{tabular}{lCl>{$\displaystyle}l<{$}}
  \toprule
  Größe & \text{Formelzeichen} & Name & \text{Einheit} \\
  \midrule
  Fläche & A & Quadratmeter & \unit{\metre\squared} \\
  Volumen & V & Kubikmeter & \unit{\metre\cubed} \\
  Dichte & \rho & & \unit{\kilogram\per\metre\cubed} \\
  Geschwindigkeit & v & & \unit{\metre\per\second} \\
  Beschleunigung & a & & \unit{\metre\per\second\squared} \\
  Kraft & F & Newton & \unit{\newton} = \unit{\kilogram\metre\per\second\squared} \\
  Druck & p & Pascal & \unit{\pascal} = \unit{\newton\per\metre\squared}
                       = \unit{\kilogram\per\second\squared\per\metre} \\
  Impuls & p & Newtonsekunde & \unit{\newton\second} = \unit{\kilogram\metre\per\second} \\
  Arbeit, Energie & W, E & Joule & \unit{\joule} = \unit{\watt\second} = \unit{\newton\metre}
                                   = \unit{\kilogram\metre\squared\per\second\squared} \\
  Leistung & P & Watt & \unit{\watt} = \unit{\joule\per\second} = \unit{\volt\ampere}
  % = \unit{\kilogram\metre\squared\per\second\cubed}
                        \\
  \midrule
  elektrische Ladung & Q & Coulomb & \unit{\coulomb} = \unit{\ampere\second} \\
  elektrisches Potential & U & Volt & \unit{\volt} = \unit{\watt\per\ampere} = \unit{\joule\per\coulomb} \\
  elektrischer Widerstand & R & Ohm & \unit{\ohm} = \unit{\volt\per\ampere} \\
  elektrischer Leitwert & G & Siemens & \unit{\siemens} = \unit{\per\ohm} \\
  \midrule
  Frequenz & f & Hertz & \unit{\hertz} = \unit{\per\second} \\
  Kreisfrequenz & \omega & Hertz & \unit{\hertz} = \unit{\per\second} \\
  Radioaktivität & A & Becquerel & \unit{\becquerel} = \unit{\per\second} \\
  Strahlendosis & D & Gray & \unit{\gray} = \unit{\joule\per\kilogram} \\
  Äquivalentdosis & H, E & Sievert & \unit{\sievert} = \unit{\joule\per\kilogram} \\
  \midrule
  Entropie & S & & \unit{\joule\per\kelvin} \\
  \midrule
  katalytische Aktivität & z & Katal & \unit{\katal} = \unit{\mole\per\second} \\
  \bottomrule
\end{tabular}
\end{center}


\subsection{Einheitenpräfixe (Auswahl)}

\begin{center}
\begin{tabular}{lcL@{\qquad\qquad}lcL}
  \toprule
  Präfix & Zeichen & \text{Faktor} & Präfix & Zeichen & \text{Faktor} \\
  \midrule
  Deka     & da    & 10^{1}    & Dezi     & d  & 10^{-1} \\
  Hekto    & h     & 10^{2}    & Zenti    & c  & 10^{-2} \\
  Kilo     & k     & 10^{3}    & Milli    & m  & 10^{-3} \\
  Mega     & M     & 10^{6}    & Mikro    & \SIUnitSymbolMicro  & 10^{-6} \\
  Giga     & G     & 10^{9}    & Nano     & n  & 10^{-9} \\
  Tera     & T     & 10^{12}   & Piko     & p  & 10^{-12} \\
  Peta     & P     & 10^{15}   & Femto    & f  & 10^{-15} \\
  \bottomrule
\end{tabular}
\end{center}

\newpage
\section{Physikalische Konstanten}
%
\begin{center}
  \sisetup{
    round-mode = places,
    round-precision = 3
  }
  \begin{tabular}{lCR}
    \toprule
    Bezeichnung & \text{Symbol} & \text{Wert (gerundet$^\dagger$)} \\
    \midrule
    Elementarladung & e & \elementarychargeqty{2} \\
    Lichtgeschwindigkeit im Vakuum & c_0  & \speedoflightqty \\
    Erdbeschleunigung & g & \standardgravityacceleration{2} \\
    Avogadro-Konstante & N_A & \avogadroconstantqty \\
    universelle Gaskonstante & R & \molargasconstantqty{3} \\
    absoluter Nullpunkt & & \qty[round-precision=2]{-273.15}{\celsius} \\
    Molvolumen eines idealen Gases (\standardatmosphereqty, \qty{0}{\celsius}) & V_m
                                & \molargasvolumeqty{1} \\
    Standard-Temperatur       &  & \qty[round-precision=0]{25}{\celsius} \\
    Standard-Atmosphärendruck & & \standardatmosphereqty \\
    Bohrscher Radius & a_0 & \bohrradiusqty \\
    atomare Masseneinheit & m & \atomicmassqty = \qty[round-precision=1]{1}{u} \\
    \bottomrule
  \end{tabular}
\end{center}

\begin{footnotesize}
  $\dagger$ Sie sollten die angegebenen Werte in (mindestens) dieser
  Präzision für Überschläge und Plausibilitätskontrollen im Kopf
  haben. Für genaue Berechnungen verwenden Sie bitte präzisere Werte aus
  anderen Quellen.
\end{footnotesize}


\section{Mechanik}

\subsection{Impuls}

\begin{formula}{p = m \cdot v}
  p & Impuls \\
  m & Masse \\
  v & Geschwindigkeit
\end{formula}


\subsection{Kraft}

\begin{formula}{F = m \cdot a}
  F & Kraft \\
  m & Masse \\
  a & Beschleunigung \\
\end{formula}

\subsection{Gewichtskraft}

\begin{formula}{F_G = m \cdot g}
  F_G & Gewichtskraft  \\
  m   & Masse          \\
  g   & 
\end{formula}


\subsection{Dichte}

\begin{formula}{\rho = \frac{m}{V}}
  \rho & Dichte  \\
  m       & Masse   \\
  V       & Volumen
\end{formula}


\subsection{Hooksches Gesetz}

\begin{formula}{F = k\cdot s}
  F  & Rückstellkraft (einer Feder) \\
  k  & Federkonstante \\
  s  & Auslenkung
\end{formula}


\subsection{Druck}

\begin{formula}{p = \frac{F}{A}}
    p & Druck \\
    F & Kraft \\
    A & Fläche
\end{formula}


\subsection{Auftrieb}

Die Auftriebskraft eines Körpers entspricht der Gewichtskraft des
verdrängten Mediums (Archimedisches Prinzip).

\begin{formula}{F_A = \rho \cdot g \cdot V}
  F_A & Auftriebskraft \\
  \rho & Dichte des Mediums \\
  g   & Erdbeschleunigung \\
  V   & Volumen des Körpers
\end{formula}


\subsection{Hydrostatischer Druck}

\begin{formula}{p = \rho \cdot g \cdot h}
  p & Druck \\
  g & Erdbeschleunigung \\
  h & Höhe der Flüssigkeitssäule \\
  \rho & Dichte der Flüssigkeit
\end{formula}


\subsection{Geschwindigkeit}

\begin{formula}{v = \frac{s}{t}}
  v & Geschwindigkeit \\
  s & zurückgelegte Strecke \\
  t & Zeit
\end{formula}


\subsection{Beschleunigung}

\begin{formula}{v = a \cdot t}
  v & Geschwindigkeit \\
  a & Beschleunigung \\
  t & Zeit
\end{formula}

\begin{formula}{s = \frac12 a \cdot t^2}
  s & zurückgelegte Strecke \\
  a & Beschleunigung \\
  t & Zeit
\end{formula}


\subsection{freier Fall}

\begin{formula}{h = \frac12 \cdot g \cdot t^2}
  h & Fallhöhe \\
  g & Erdbeschleunigung \\
  t & Zeit
\end{formula}

\begin{formula}{v = \sqrt{2\cdot h \cdot g}}
  v & Fallgeschwindigkeit \\
  h & Fallhöhe \\
  g & Erdbeschleunigung \\
\end{formula}


\subsection{Frequenz}

\begin{formula}{ f = \frac1{T} }
  f & Frequenz \\
  T & Periodendauer \\
\end{formula}

\pagebreak
Für elektromagnetische Wellen gilt:
\nobreak
\begin{formula}{ E = h \cdot f }
  E & Energie \\
  h & Plancksches Wirkungsquantum \\
    & \planckconstantdef{2} \\
  f & Frequenz
\end{formula}


\subsection{Winkelgeschwindigkeit}

\begin{formula}{\omega = 2\cdot\pi\cdot f}
  \omega & Winkelgeschwindigkeit \\
  f & Frequenz \\
  \pi & \pidef
\end{formula}


\subsection{Mechanische Arbeit}

\begin{formula}{W = F \cdot s}
  W & Arbeit \\
  F & Kraft \\
  s & Weg
\end{formula}


\subsection{Hubarbeit -- potentielle Energie}

\begin{formula}{W = F_G \cdot h}
  W & Arbeit   \\
  h & Hubhöhe  \\
  F & Kraft
\end{formula}


\subsection{Beschleunigungsarbeit -- kinetische Energie}

\begin{formula}{W = \frac12 \cdot m \cdot v^2}
  W & kinetische Energie \\
  m & Masse \\
  v & Geschwindigkeit
\end{formula}


\section{Elektrizitätslehre}

\subsection{Stromstärke}

\begin{formula}{I = \frac{Q}{t}}
    I & Stromstärke \\
    Q & Ladungsmenge \\
    t & Zeit
\end{formula}


\subsection{Ohmsches Gesetz}

\begin{formula}{U = R\cdot I}
  U & elektrisches Potential, Spannung  \\
  R & elektrischer Widerstand \\
  I & Stromstärke
\end{formula}


\subsection{elektrische Leistung}

\begin{formula}{P = U\cdot I}
  P & Leistung \\
  U & elektrisches Potential, Spannung \\
  I & Stromstärke 
\end{formula}

\newpage
\subsection{elektrische Arbeit}

\begin{formula}{W = U \cdot I \cdot t}
  W & Arbeit, Energie \\
  U & elektrisches Potential, Spannung \\
  I & Stromstärke \\
  t & Zeit
\end{formula}


\subsection{Coulombsches Gesetz}

\begin{formula}{F = \frac{1}{4\pi\varepsilon_0} \cdot \frac{Q_1 \cdot Q_2}{r^2}}
  F       & Kraft \\
  Q_1, Q2 & beteiligte Ladungen \\
  r       & Abstand der Ladungen voneinander \\
  \pi     & \pidef \\
  \varepsilon_0 & elektrische Feldkonstante \\
          & ($\varepsilon_0 \approx \vacuumelectricpermittivityqty{0}$)
\end{formula}


\section{Wärmelehre}
        
\subsection{Zustandsgleichung idealer Gase}

\begin{formula}{p \cdot V = \nu \cdot R \cdot T}
  p & Druck \\
  V & Volumen \\
  \nu & Stoffmenge \\
  R & universelle Gaskonstante \\
      & ($ R \approx \molargasconstantqty $) \\
  T   & absolute Temperatur
\end{formula}

\section{Atomphysik}

\subsection{Molekülmasse}

\begin{formula}{m = \nu \cdot M}
  m & Masse \\
  \nu & Stoffmenge \\
  M   & Molekülmasse
\end{formula}


\subsection{Radioaktivität}

\begin{formula}{N(t) = N_0 \cdot e^{-\lambda t}}
  N(t) & Anzahl nicht zerfallener Atomkerne \\
       & nach der Zeit~$t$ \\
  N_0  & Anzahl nicht zerfallener Atomkerne \\
       & zum Zeitpunkt~$t=0$ \\
  e    & Eulersche Zahl \\
  & ($e \approx \eulernumberqty$) \\
  \lambda & Zerfallsgeschwindigkeitskonstante \\
  t & Zeit
\end{formula}

\begin{formula}{t_{1/2} = \frac{\ln2}{\lambda}}
  t_{1/2} & Halbwertszeit \\
  \lambda & Zerfallsgeschwindigkeitskonstante
\end{formula}


\begin{appendix}

\section{Griechisches Alphabet}

\begin{center}
\begin{tabular}{lCC}
  \toprule
  Name & \text{Großbuchstabe} & \text{Kleinbuchstabe} \\
  \midrule
  Alpha   & A        & \alpha \\
  Beta    & B        & \beta  \\
  Gamma   & \Gamma   & \gamma \\
  Delta   & \Delta   & \delta \\
  Epsilon & E        & \epsilon, \varepsilon \\
  Zeta    & Z        & \zeta \\
  Eta     & H        & \eta  \\
  Theta   & \Theta   & \theta \\
  Iota    & I        & \iota  \\
  Kappa   & K        & \kappa \\
  Lambda  & \Lambda  & \lambda \\
  My      & M        & \mu \\
  Ny      & N        & \nu \\
  Xi      & \Xi      & \xi \\
  Omikron & O        & o \\
  Pi      & \Pi      & \pi      \\
  Rho     & P        & \rho, \varrho \\
  Sigma   & \Sigma   & \sigma, \varsigma \\
  Tau     & T        & \tau \\
  Ypsilon & Y        & \upsilon \\
  Phi     & \Phi     & \phi, \varphi \\
  Chi     & X        & \chi \\
  Psi     & \Psi     & \psi          \\
  Omega   & \Omega   & \omega        \\
  \bottomrule
\end{tabular}
\end{center}

% https://www.youtube.com/watch?v=-4z_OHMuy80
% https://www.youtube.com/watch?v=LCHbcMOm4mU


\section{Quellen}

\begin{thebibliography}{9}
\bibitem{SI2019}Bureau International des Poids et Mesures, \textsl{The International System of Units (SI)}, 9th edition, 2019, ISBN 978-92-822-2272-0
\bibitem{CODATA2018}National Institute of Standards and Technology, \textsl{CODATA Recommended Values of the Fundamental Physical Constants: 2018}, NIST SP 961, Mai 2019, \url{https:physics.nist.gov/constants}
% \bibitem{Fersch2020}Klemens Fersch, \textsl{Formelsammlung Physik}, 2020, \url{https://fersch.de}
\bibitem{Tipler2015}Paul A. Tipler, Gene Mosca, \textsl{Physik für Wissenschaftler und Ingenieure}, 7. deutsche Auflage,
  Springer Spektrum, Heidelberg 2015, ISBN 978-3-642-554165-0
\end{thebibliography}

\end{appendix}
\end{document}
